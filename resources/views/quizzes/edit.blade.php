@extends('layout')

<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
</head>
<body>
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Quizzes / {{$quiz->name}}</h1>
    </div>
@endsection

@section('content')
    <div class="row">
      <div class="col-md-12">
        <form action="{{ route('quizzes.update', $quiz->id) }}" method="POST">
            @if($quiz->quiz_type == 0)
              <input type="radio" value="0" name="quiz_type" checked="">Conventional<br>
              <input type="radio" value="1" name="quiz_type">Live<br>
            @else
              <input type="radio" value="0" name="quiz_type">Conventional<br>
              <input type="radio" value="1" name="quiz_type" checked="">Live<br>
            @endif
          <!-- <p>Already shared to: </p>
          <div>
             @foreach($sharedTo as $shared)
              {{$shared->user_email}}
             @endforeach
          </div> -->

          <p>Share quiz to other users: </p>
          <select name="visibility[]" class="selectpicker" data-live-search="true" multiple>
            @foreach($users as $user)
                <option value="{{$user->email}}">{{$user->name}} {{$user->email}}</option>
            @endforeach
          </select>
              <input type="hidden" name="_method" value="PUT">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              @include('quizzes._quiz')
              
              <div class="well well-sm">
                  <button type="submit" class="btn btn-primary">Save</button>
               
                  <a class="btn btn-link pull-right" href="{{ route('quizzes.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
              </div>
        </form>
        @if($sections)
          @foreach($sections as $section)
          <div>
            <p>{{$section->name}}</p>
            @foreach($section->questions()->get() as $question)
             @include('questions._show', ['question' => App\Question::getQuestion($question->question_id)])
            @endforeach
             <div id="<?php echo "section" . $section->id?>" ondrop="drop(event)" class="section" ondragover="allowDrop(event)" style="outline-width: 10; outline-color: black; border: solid black">Drop the questions here</div>
            <button onclick="getQuestions('section'+{{$section->id}})">Save</button>
          </div>
          @endforeach
        @endif
        <div class="row">
          <h4>Available Questions</h4>
          @foreach($questions as $question)
           <div id="<?php echo $question->id ?>" draggable="true" ondragstart="drag(event)" class = "question"> {!!$question->question!!} </div><br>
          @endforeach
          {!! $questions->render() !!}
        </div>
        
         <div class="input-group">    
            <button id = "myBtn" class = 'btn btn-success'>Add Section</button>
         </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
       <!--  <div class="panel panel-success">
            <div class="panel-heading">Questions</div>
            <div class="panel-body">
              @foreach($quiz->questions as $question)
                @include('questions._show', ['question' => $question])
              @endforeach
            </div>
        </div> -->
      </div>
    </div></br>
    <div class="row">
      <div class="col-md-12">
          <!-- <div class="panel panel-info"> -->
            <!-- <div class="panel-heading" style="height:50px;">Available Questions
              <form class="form-inline pull-right">
                <div class="form-group">
                  <input type="text" class="form-control input-sm" id="search-field" placeholder="easy">
                </div>
                <button type="submit" class="btn input-sm btn-default">Search</button>
              </form> -->
            <!-- </div> -->
            <!-- <div class="panel-body"> -->
            <!--    @include('questions._index', ['questions' => $questions, 'quiz' => $quiz]) -->
            <!-- </div> -->
          <!-- </div> -->
      </div>
    </div>
    @include('quizzes.add_section_modal')
    <script type="text/javascript">
     
    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    btn.onclick = function() {
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    function drag(ev) {
      ev.dataTransfer.setData("text", ev.target.id);
    }

    function allowDrop(ev) {
      ev.preventDefault();
    }
    function drop(ev) {
      console.log("hello");
      ev.preventDefault();
      var data = ev.dataTransfer.getData("text");
      var name = ev.target.className;
      console.log(name);
      if(name=='section'){
        ev.target.appendChild(document.getElementById(data));
      }
      else {
        console.log(ev.target.parentElement);
        ev.target.parentElement.appendChild(document.getElementById(data));
      }
      
    }
    function getQuestions(sectionID) {
      var parent = document.getElementById(sectionID);
      var children = parent.childNodes;
      var questions = [];
      console.log(children.length);
      for(i = 0, j=0; i<children.length; i++){
        if(children[i].className == "question"){
          questions[j++] = children[i].id;
        }
      }
      token = $('meta[name="csrf-token"]').attr('content');
      data = { "_token": "{{ csrf_token() }}", section_id: sectionID, questions: questions};
      
      $.ajax({
          type: "POST",
          url: "{{ route('quizzes.savesectionquestions') }}",
          data: data,
           async : true, // if you want wait until ajax done, you should set "async" false
          complete : function(){
              console.log("[JQUERY AJAX COMPLETE]");
          },
          success: function(response){ // What to do if we succeed
              alert("Submitted");
          },
          error: function(xhr, status, error) {
            alert("FAIL");
          }
      });
      console.log("huhu");
      //igkuha nala tanan an child of child of child
    }
    </script>
    <meta name="_token" content="{!! csrf_token() !!}" />
@endsection
@section('scripts')
 <!--  <script src="//code.jquery.com/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.input-group.date').datepicker({
      format: "yyyy-mm-dd",
      todayBtn: true,
      autoclose: true
    });
  </script>
@endsection
</body>
