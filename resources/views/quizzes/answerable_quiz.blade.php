<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
</head>
<body>
  @extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Quizzes / {{$quiz->name}}</h1>
    </div>
@endsection

@section('content')
    <!--@include('error')-->

    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('quizzes.update', $quiz->id) }}" method="POST">
            <label>Already shared to: </label>
            <div>
               @foreach($sharedTo as $shared)
                {{$shared->user_email}}
                @endforeach
            </div>
            <label>Share quiz to other users: </label>
            <select name="visibility[]" class="selectpicker" data-live-search="true" multiple>
              @foreach($users as $user)
                  <option value="{{$user->email}}">{{$user->name}} {{$user->email}}</option>
              @endforeach
            </select>
            <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @include('quizzes._quiz')
                
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                 
                    <a class="btn btn-link pull-right" href="{{ route('quizzes.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">Questions</div>
            <div class="panel-body">
              @foreach($quiz->questions as $question)
                @include('questions._show', ['question' => $question])
              @endforeach
            </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
          <div class="panel panel-info">
            <div class="panel-heading" style="height:50px;">Available Questions
              <form class="form-inline pull-right">
                <div class="form-group">
                  <input type="text" class="form-control input-sm" id="search-field" placeholder="easy">
                </div>
                <button type="submit" class="btn input-sm btn-default">Search</button>
              </form>
            </div>
            <div class="panel-body">
               @include('questions._index', ['questions' => $questions, 'quiz' => $quiz])
            </div>
          </div>
      </div>
    </div>
@endsection
@section('scripts')
  <script src="//code.jquery.com/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.input-group.date').datepicker({
      format: "yyyy-mm-dd",
      todayBtn: true,
      autoclose: true
    });
  </script>
@endsection
</body>
