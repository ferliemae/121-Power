@extends('layout')
@section('header')
<h2 class="fnt note rounded" style="width: 35%">Result for Question {{$quiz->curr_question}}</h2>

<br><br>
    <div class="row">
		@if(!empty($answers))
			@if($quiz->curr_round!="Difficult")
		<div class="col-md-12 tbl-header">
	 		<table class="tbl" cellpadding="0" cellspacing="0" border="0">
	 			<thead>
					<tr>
						<th>Name of Participants</th>
						<th style="text-align: center">Correct or Wrong</th>
						<!-- <th style="text-align: center">Points</th> -->
					</tr>
				</thead>
            </table>
        </div>

        <div style="height: 400px;" class="tbl-content">
            <table class="tbl" cellpadding="0" cellspacing="0" border="0">
            	 <tbody>
					@foreach($answers as $answer)
					<tr>
						<td>&nbsp;&nbsp;&nbsp;{!!\Helper::user_name($answer->user_id);!!}</td>
						<td style="text-align: center">
							@if($answer->points!=0)
								<a class="btn btn-xs btn-success" >
                                <i class="glyphicon glyphicon-ok" style="font-size:25px;"></i></a>
							@else
								 <a class="btn btn-xs btn-danger" >
                                <i class="glyphicon glyphicon-remove" style="font-size:25px;"></i></a>
							@endif
						</td>
						<!-- <td style="text-align: center">{{$answer->points}}</td> -->
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
	@else
		@include('answerquizzes.check_manually', ['pairs' => $pairs, 'question_id' => $question->id, 'quiz_id' => $quiz->id, 'round' => $quiz->curr_round, 'question_number' => $quiz->curr_question])
	@endif
@endif

<div class="panel-body"><br>
	<a class="bb" style="" href="{{route('quizzes.nextquestion', $quizID)}}">Next</a>
</div>
@endsection
