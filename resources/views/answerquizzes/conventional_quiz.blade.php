@extends('layout')
@section('header')
<div class="page-header">
  <h1 class="fnt note rounded" style="width: 35%">
    <i class="glyphicon glyphicon-pushpin" aria-hidden="true"></i> {{$quiz->name}} Quiz
  </h1> 
  <a style="position: absolute; right: 190px; top: 150px;" class="btn btn-primary" href="{{ route('quizzes.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
  <form action="{{ route('quizzes.destroy', $quiz->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
      <input type="hidden" name="_method" value="DELETE">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
  </form>
</div>
@endsection

@section('content')
<form action="{{ route('user.createAnswers', ['quiz_id' => $quiz->id]) }}" method="POST">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-success">
          @foreach($quiz->questions as $question)
            <div class="panel-heading w">
              {!!$question->question!!}
            </div>
            <div class="panel-body">
              @foreach($question->options as $option)
                <li class="list-group-item ss">
                  <input type="radio" name="<?php echo $question->id ?>" value="<?php echo $option->id ?>"> <?php echo strip_tags($option->value)?>
                </li>
              @endforeach
            </div>
          @endforeach
        </div>
      </div>
    </div>
    <center><input class="bb" style="margin: 0 0 0 0px; width: 15%;"type="submit"></center>
</form>
@endsection