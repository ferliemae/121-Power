@extends('layout')
<style>
    .panel-bod {
        padding: 0;
    }
</style>

@section('content')
        <br>
        <div id="try"></div>
        <h3 class="fnt note rounded" style="width: 40%; font-size: 30px" id="question-label"></h3><br><br><br> 
          <p id="demo"></p>
        <div class="panel panel-success">
            <div class="panel-heading w">
                <div id="result"></div>        
            </div>  
            <div>
                <ul Class="panel-bod" id="options"></ul>
            </div>    
        </div><br>
        <a class="bb" id="submit" style="margin: 0px 45%;" onclick="submitAnswer(event);">Submit</a>
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> 
    <script>
        var questionId = -1;
        var submitted = false;
        var selected = -1;
        var answer = "";
        var round = "";
        var sagot = "";
        if(typeof(EventSource) !== "undefined") {
            var url = "/updateQuestion?id=" + {{$quiz->id}};
            var source = new EventSource(url);
            source.onmessage = function(event) {
                var question = JSON.parse(event.data);
                console.log(question);
                if(question.isMsg){   
                    if(question.question == 1){
                        
                        document.getElementById("question-label").style.display = "none";
                        document.getElementById("submit").style.display = "none";
                        document.getElementById("result").innerHTML = "<h1>  Your Answer is " + answer.replace(/<(?:.|\n)*?>/gm, '') +   "</h1>";
                        MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("result")]);
                    }else{
                        document.getElementById("question-label").style.display = "none";
                        document.getElementById("result").innerHTML = question.question;
                        document.getElementById("options").style.display = "none";
                        document.getElementById("submit").style.display = "none";
                    }
                }
                if(!question.isMsg ){
                    console.log(question.isMsg);
                    
                    if(submitted == true && questionId == question.id || JSON.parse(event.data).showing == 1 ){
                        document.getElementById("submit").style.display = "none";
                        document.getElementById("result").innerHTML = "<h1>  Your Answer is " + answer.replace(/<(?:.|\n)*?>/gm, '') +   "</h1>";
                        MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("result")]);
                    }
                    if(questionId != question.id ){
                        sagot = "";
                        document.getElementById("question-label").style.display = "block";
                        document.getElementById("submit").style.display = "inline";
                        document.getElementById("question-label").innerHTML=question.round + " Round: Question No. " ;
                        document.getElementById("options").style.display = "block";
                        document.getElementById("result").innerHTML = question.question + "<br>";
                        MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("result")]);
                        var options = JSON.parse(event.data).options;
                        document.getElementById("options").innerHTML='';
                        var questionHtml = '';
                        var y = 65;
                        if(question.round != "Difficult"){
                            for (var i = 0; i<options.length; i++){
                                questionHtml += '<li class="ss" value="' +  options[i].id + '" onclick="changeSelect(this.value)"><input  type="radio" name="option" '
                                questionHtml += 'value="' + options[i].value + '" id="' + options[i].id +  '"/>'
                                questionHtml += "<label >" + String.fromCharCode(y++) + ".&nbsp;&nbsp;" + options[i].value.replace(/<(?:.|\n)*?>/gm, '') + '</label></li>'
                            }
                            document.getElementById("options").innerHTML=questionHtml;
                        }else{
                            document.getElementById("options").innerHTML+='<textarea id="answer" class="form-control" ><\/textarea>'
                        }
                        MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("options")]);
                        round = question.round;
                        questionId = question.id;
                        submitted = false;
                        answer = "";
                    }

                }
                
            };
        } else {
            document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
        }
        function submitAnswer(e){
            token = $('meta[name="csrf-token"]').attr('content');
            if(round == "Difficult"){
                selected = "";
                answer=  document.getElementById("answer").value;
            }else{
                selected = $('input[name=option]:checked').attr('id');
                answer = $('input[name=option]:checked').val();
            }
            if((selected && round!="Difficult") || round=="Difficult"){
                submitted = true;
                $.ajax({
                    type: "POST",
                    url: "{{ route('user.answers') }}",
                    data: { "_token": "{{ csrf_token() }}", quiz_id: {{ $quiz->id }}, answer: selected, question_id: questionId, user_id: {{ $user->id }}, round : round, sagot: answer}
                })
            }else{
                alert("Please select an answer") 
                submitted = false; 
            }
        }

        function changeSelect(id){
            radiobtn = document.getElementById(id);
            radiobtn.checked = true;
        }
</script>
<script src='https://code.responsivevoice.org/responsivevoice.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
    <script>
        var socket = io('http://172.16.5.173:8080'); //change to ip
        socket.on("presence-receiveQuestion" + {{ $quiz->id }} + ":App\\Events\\NextQuestion", function(message){
            // increase the power everytime we load test routereceiveQuestion
            
            if(message.question === -1){
                 document.getElementById("question-label").style.display = "none";
                 document.getElementById("submit").style.display = "none";
                 document.getElementById("result").innerHTML = "<h1>  Your Answer is " + answer.replace(/<(?:.|\n)*?>/gm, '') +   "</h1>";
                 MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("result")]);
            }else if(message.question === 0){
                  document.getElementById("question-label").style.display = "none";
                  document.getElementById("result").innerHTML = "Please wait for the next round to start.";
                  document.getElementById("options").style.display = "none";
                  document.getElementById("submit").style.display = "none";
            }else{ 
                var question = message.question;
                sagot = "";
                document.getElementById("question-label").style.display = "block";
                document.getElementById("submit").style.display = "inline";
                document.getElementById("question-label").innerHTML=question.round + " Round: Question No. " + message.question_no;
                document.getElementById("options").style.display = "block";
                document.getElementById("result").innerHTML = question.question + "<br>";
                MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("result")]);
                var options = message.options;
                document.getElementById("options").innerHTML='';
                var questionHtml = '';
                var y = 65;
                if(question.round != "Difficult"){
                    for (var i = 0; i<options.length; i++){
                        questionHtml += '<li class="ss" value="' +  options[i].id + '" onclick="changeSelect(this.value)"><input  type="radio" name="option" '
                        questionHtml += 'value="' + options[i].value + '" id="' + options[i].id +  '"/>'
                        questionHtml += "<label >" + String.fromCharCode(y++) + ".&nbsp;&nbsp;" + options[i].value.replace(/<(?:.|\n)*?>/gm, '') + '</label></li>'
                    }
                    document.getElementById("options").innerHTML=questionHtml;
                }else{
                    document.getElementById("options").innerHTML+='<textarea id="answer" class="form-control" ><\/textarea>'
                }
                MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("options")]);
                round = question.round;
                questionId = question.id;
                submitted = false;
                answer = "";
            }
        });
         socket.on("presence-updateTime" + {{ $quiz->id }} + ":App\\Events\\UpdateTime", function(message){
            // increase the power everytime we load test routereceiveQuestion
            var distance = message.time;
            var days = Math.floor(distance / (60 * 60 * 24));
            var hours = Math.floor((distance % ( 60 * 60 * 24)) / ( 60 * 60));
            var minutes = Math.floor((distance % ( 60 * 60)) / (60));
            var seconds = Math.floor((distance % (60)));
            if (typeof responsiveVoice != 'undefined') {
    console.log('ResponsiveVoice already loaded');
    console.log(responsiveVoice);
}
             if(distance == 10){
                responsiveVoice.speak("Last 10 seconds");
            }
            // Display the result in the element with id="demo"
            document.getElementById("demo").innerHTML = days + "d " + hours + "h "
            + minutes + "m " + seconds + "s ";

            // If the count down is finished, write some text 
            if (distance <= 0) {
                responsiveVoice.speak("TIME'S UP");
                document.getElementById("demo").innerHTML = "TIME'S UP";
                document.getElementById("question-label").style.display = "none";
                document.getElementById("submit").style.display = "none";
                document.getElementById("result").innerHTML = "<h1>  Your Answer is " + answer.replace(/<(?:.|\n)*?>/gm, '') +   "</h1>";
                MathJax.Hub.Queue(["Typeset",MathJax.Hub,document.getElementById("result")]);
            }
           
            //  document.getElementById("try").innerHTML = message.question.question;
            
        });
    </script>
<meta name="_token" content="{!! csrf_token() !!}" />
<link type="text/css" href="/css/an.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
