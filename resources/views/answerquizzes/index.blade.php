@extends('layout')

@section('header')
    @if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
    @endif
    <div class="page-hdr clearfix">
        <h1 class="fnt note rounded" style="width: 35%">
          <i class="glyphicon glyphicon-check" aria-hidden="true"></i> ANSWER QUIZZES
        </h1> 
            <form class="form-inline pull-right">
        <div class="form-group">
          <input type="text" class="form-control input-sm" id="search-field" placeholder="Search">
        </div>
        <div class="form-group">
            <button type="submit" class="btn input-sm btn-default">Search</button>
            <a class="btn btn-success input-sm" href="{{ route('quizzes.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </div>  
    </form>

    </div>
@endsection

@section('content')
    <br><div class="row">
    @if($quizzes->count())
        <div class="col-md-12 tbl-header">
           
                <table class="tbl" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NAME</th>
                            <th>Schedule</th>
                            <th>OPTIONS</th>
                        </tr>
                    </thead>
            </table>
        </div>

        <div class="tbl-content">
            <table class="tbl" cellpadding="0" cellspacing="0" border="0">
                <tbody>
                    @foreach($quizzes as $quiz)
                        <tr>
                            <td>{{$quiz->id}}</td>
                            <td>{{$quiz->name}}</td>
                            <td>{{$quiz->schedule}}</td>
                            <td>
                                <a class="btn btn-xs btn-primary" href="{{route('answerquizzes.welcome', $quiz->id)}}"><i class="glyphicon glyphicon-eye-open"></i>Answer</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
                {!! $quizzes->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection