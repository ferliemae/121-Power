<div class="row">
@if($questions->count())
    <div class="col-md-12 tbl-header">
       
        <table class="tbl" cellpadding="0" cellspacing="0" border="0">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>QUESTION</th>
                    <th>ROUND</th>
                    <th>OPTIONS</th>
                </tr>
            </thead>
        </table>
    </div>
   
    <div class="tbl-content">
        <table class="tbl" cellpadding="0" cellspacing="0" border="0">
            <tbody>
                @foreach($questions as $question)
                    <tr>
                        <td>{{$question->id}}</td>
                        <td>{!! $question->question !!}</td>
                        <td>{{$question->round}}</td>
                        <td>
                                @if(isset($quiz))
                                    @include('questions._controladd')
                                @else
                                    @include('questions._controlall')
                                @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
            {!! $questions->render() !!}
             @else
            <h3 class="text-center alert alert-info">Empty!</h3>
        @endif

    </div>
</div>