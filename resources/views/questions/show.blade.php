@extends('layout')
@section('header')
<div class="page-header">
        <h1>Question # {{$question->id}}</h1>
</div>
@endsection

@section('content')
    @include('questions._show', ['question'=>$question])
@endsection