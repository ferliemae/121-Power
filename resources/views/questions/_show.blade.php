<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-9">{!! $question->question !!}</div>
                    <div class="col-md-3">
                        <div class="btn-group pull-right" role="group" aria-label="...">
                            <a class="btn btn-xs btn-warning btn-group" role="group" href="{{ route('questions.edit', $question->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>

                            @if(isset($quiz))
                            <a class="btn btn-xs btn-danger" role="group" href="{{ route('quizzes.removequestion', ['id'=>$quiz->id, 'question'=>$question->id]) }}"><i class="glyphicon glyphicon-trash"></i> Remove From Quiz</a>
                            @endif
                           
                        </div>
                    </div>    
                </div>
                       
            </div>
            <div class="panel-body">
                <ul class="list-group-item">
                    @foreach($question->options as $option)
                    <li class="list-group-item">{!! $option->value !!}</li>
                    @endforeach
                </ul>
            </div>
        </div> 
    </div>
</div>