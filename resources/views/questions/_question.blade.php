

<div class="form-group @if($errors->has('round')) has-error @endif">
   <label style="font-size: 15px;" for="round-field">ROUND</label>
   <select onchange="round1()" id="round" name="round" class="form-control" style="width: 30%;">

   		<option {{$question->round == 'Easy' ? 'selected': ''}}>Easy</option>
   		<option {{$question->round == 'Average' ? 'selected': ''}}>Average</option>
   		<option {{$question->round == 'Difficult' ? 'selected': ''}}>Difficult</option>
   </select>
   <label>POINTS</label>
   <input type="number" name="points">
   @if($errors->has("round"))
    <span class="help-block">{{ $errors->first("round") }}</span>
   @endif
</div>

<label style="font-size: 15px;" for="question-field">QUESTION</label>
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="form-group @if($errors->has('question')) has-error @endif">
   
        <textarea id="question-field" name="question" class="form-control" >
        {{is_null(old("question")) ? $question->question : old("question") }}
        </textarea>
        @if($errors->has("question"))
          <span class="help-block">{{ $errors->first("question") }}</span>
        @endif
    </div>
  </div>
</div>
<div id="answer" style="display: none;" >
  <label style="font-size: 15px;" for="question-field">Answer</label>
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="form-group @if($errors->has('question')) has-error @endif">
    
        
          <textarea name="answer" class="form-control" >
             {{ $question->answer }}
          </textarea>
          
      </div>
    </div>
  </div>
</div>
<script>
    $('textarea').ckeditor();
    // $('.textarea').ckeditor(); // if class is prefered.
    function round1(){
       var yourSelect = document.getElementById( "round" );
       var answerArea = document.getElementById( "answer" );
       var options = document.getElementById( "options" );
       var round = yourSelect.options[ yourSelect.selectedIndex ].value;
       if(round == "Difficult" ){
           answerArea.style.display = "block";
           options.style.display = "none";
       }else{
           answerArea.style.display = "none";
           options.style.display = "block";
        }
     } 
  </script>
