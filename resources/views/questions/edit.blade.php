@extends('layout')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <style>
        .bttn {
            position: absolute;
            top: 25px;
            right: 95px;
        }
        .btton {
            position: absolute;
            top: 25px;
            right: 15px;
        } 
        .bttonn {
            position: absolute;
            top: 30px;
            right: 165px;
        }   
    </style>
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Questions / Edit #{{$question->id}}</h1>
    </div>
@endsection

@section('content')
    <!--@include('error')-->

    <div class="row">
        <div class="col-md-12">



            <form action="{{ route('questions.update', ['question'=>$question->id]) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @include('questions._question')
                
                <!-- <div class="well well-sm"> -->
                <label style="font-size: 15px;" class="bttonn" for="question-field">Save this question?</label>
                    <button type="submit" class="btn bttn btn-success">Save</button>
                    <a class="btn btton btn-primary pull-right" href="{{ route('questions.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                <!-- </div> -->
            </form>
        </div>
    </div><br>
    @if($question->round != "Difficult")
       <div id="options" style="display: block;">
        <label style="font-size: 15px;" for="question-field">OPTIONS</label>
            <div class="panel panel-default">
                <div class="panel-body">
                    @foreach($question->options as $option)
                        @include('options._edit', ['option'=>$option, 'question_id'=>$option->question_id])
                    @endforeach
                    @include('options._create', ['option'=>$question->blankOption(), 'question_id'=>$question->id])
                </div>
            </div>
        </div>
    @endif
@endsection
@section('scripts')
  <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
  <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
  <script>
    $('.date-picker').datepicker({

    }); 

  </script>
@endsection
