
<style>
    .wll {
        padding: 7px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
          background-color: #f5f5f5;
        border: 1px solid #e3e3e3;
        width: 15%;
        margin: 0 15px;
    }
    .w {
        margin-bottom: 45px;
    }
</style>
<div class="row">
  <div class="col-md-12">

      <form action="{{ route('questions.options.update', ['question'=>$question_id, 'option'=>$option->id]) }}" method="POST">
          <input type="hidden" name="_method" value="PUT">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">

          @include('options._option')


          <div class="wll w well-sm">

              <button type="submit" class="btn btn-primary">Save</button>               
         
      </form>
         <form action="{{ route('questions.options.destroy', ['question'=>$question_id, 'option'=>$option->id]) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                  <input type="hidden" name="_method" value="DELETE">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <button type="submit" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
              </form>
          </div>
  </div>
</div>