-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 24, 2017 at 02:44 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `csquizapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `lq_answers`
--

CREATE TABLE `lq_answers` (
  `id` int(10) UNSIGNED NOT NULL,
  `quiz_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `answer_id` int(11) DEFAULT NULL,
  `points` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `answer` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_09_27_062232_create_quizzes_table', 1),
(4, '2016_09_27_064434_add_date_to_quizzes_table', 1),
(5, '2016_09_27_071155_change_date_in_quizzes_to_date', 1),
(6, '2016_09_27_084319_create_questions_table', 1),
(7, '2016_09_28_042914_drop_quiz_id_from_questions', 1),
(8, '2016_09_28_044643_change_question_to_text', 1),
(9, '2016_09_28_045913_create_options_table', 1),
(10, '2016_09_28_060817_add_question_to_option', 1),
(11, '2016_09_28_080753_create_table_question_quiz', 1),
(12, '2016_09_29_094727_change_option_to_text', 1),
(13, '2017_10_04_064832_add_user_id_to_quiz', 1),
(14, '2017_10_04_070236_question_casccade_delete', 1),
(15, '2017_10_04_071701_add_question_id_to_options_with_cascade_constraints', 1),
(16, '2017_10_04_072402_add_question_id_to_options_with_cascade_constraints1', 1),
(17, '2017_10_04_072735_drop_question_id', 1),
(18, '2017_10_04_072807_drop_question_id1', 1),
(19, '2017_10_04_072931_add_question_id_to_options_with_cascade_constraints2', 1),
(20, '2017_10_04_233519_add_user_id_to_questions', 1),
(21, '2017_10_05_060342_create_quiz_privacy_table', 1),
(22, '2017_10_17_135529_add_quiz_type_and_time_limit_to_quizzes_table', 1),
(23, '2017_10_18_070905_add_curr_question_and_curr_round_to_quizzes_to_quizzes_table', 1),
(24, '2017_10_19_125918_add_status_to_quizzes_table', 1),
(25, '2017_10_20_043229_create_lq_answers_table', 1),
(26, '2017_10_21_115151_add_answer_to_lqanswers', 1),
(27, '2017_10_21_115354_drop_answer_id_foreign_lqanswers', 1),
(28, '2017_10_21_140744_add_answer_to_question', 1),
(29, '2017_10_21_161121_change_answer_in_lq_answers', 1),
(30, '2017_10_21_161421_change_answer_id_in_lq_answers', 1),
(31, '2017_10_22_134705_add_showing_answer_to_quiz', 2);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `answer` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `question_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `value`, `answer`, `created_at`, `updated_at`, `question_id`) VALUES
(8, '1\r\n', 0, '2017-10-22 03:27:19', '2017-10-22 03:27:19', 18),
(9, '  \r\n2', 1, '2017-10-22 03:27:25', '2017-10-22 03:27:25', 18),
(10, '3', 0, '2017-10-22 03:27:31', '2017-10-22 03:27:31', 18),
(11, '  4\r\n', 1, '2017-10-22 03:27:45', '2017-10-22 03:27:45', 18),
(12, '  4\r\n', 1, '2017-10-22 03:28:15', '2017-10-22 03:28:28', 19),
(13, ' 5', 0, '2017-10-22 03:28:20', '2017-10-22 03:28:20', 19),
(14, '6', 0, '2017-10-22 03:28:34', '2017-10-22 03:28:34', 19),
(15, '7', 0, '2017-10-22 03:28:38', '2017-10-22 03:28:38', 19),
(16, '<p>-1</p>\r\n', 1, '2017-10-22 07:20:39', '2017-10-22 07:20:39', 21),
(17, '<p>1</p>\r\n', 0, '2017-10-22 07:20:50', '2017-10-22 07:20:50', 21),
(18, '<p>2</p>\r\n', 0, '2017-10-22 07:20:57', '2017-10-22 07:20:57', 21),
(19, '<p>4</p>\r\n', 0, '2017-10-22 07:21:06', '2017-10-22 07:21:06', 21),
(20, '<p>10</p>\r\n', 0, '2017-10-22 07:21:38', '2017-10-22 07:21:38', 22),
(21, '<p>12</p>\r\n', 1, '2017-10-22 07:23:16', '2017-10-22 07:23:16', 22),
(22, '<p>49</p>\r\n', 1, '2017-10-22 07:25:46', '2017-10-22 07:25:46', 23),
(23, '<p>59&nbsp;</p>\r\n', 0, '2017-10-22 07:25:57', '2017-10-22 07:25:57', 23),
(24, '<p>11</p>\r\n', 0, '2017-10-22 07:26:03', '2017-10-22 07:26:03', 23),
(25, '<p>24124</p>\r\n', 0, '2017-10-22 07:26:09', '2017-10-22 07:26:09', 23),
(26, '<p>7</p>\r\n', 0, '2017-10-22 07:27:04', '2017-10-22 07:27:04', 24),
(27, '<p>8</p>\r\n', 0, '2017-10-22 07:27:14', '2017-10-22 07:27:14', 24),
(28, '<p>9</p>\r\n', 0, '2017-10-22 07:27:21', '2017-10-22 07:27:21', 24),
(29, '  fasf\r\n', 1, '2017-10-23 07:55:27', '2017-10-23 07:55:27', 27),
(30, '   $3x^4+2x^3−2x^2+x+1$\r\n', 0, '2017-10-23 08:07:59', '2017-10-23 08:08:48', 28),
(31, '  $3x^4+2x^3−2x^2−x+9$\r\n', 0, '2017-10-23 08:09:41', '2017-10-23 08:09:41', 28),
(32, '  $3x^4+6x^3−2x^2+x+1$\r\n', 0, '2017-10-23 08:10:12', '2017-10-23 08:10:12', 28),
(33, '  $3x^4+6x^3−2x^2−x+1$\r\n\r\n', 1, '2017-10-23 08:10:43', '2017-10-23 08:10:53', 28),
(34, '$4x−3y=−22$\r\n', 0, '2017-10-23 08:14:54', '2017-10-23 08:14:54', 29),
(35, '$−3x+4y=20$\r\n', 1, '2017-10-23 08:15:19', '2017-10-23 08:15:19', 29),
(36, '$4x−3y=−18$\r\n', 0, '2017-10-23 08:15:38', '2017-10-23 08:15:38', 29),
(37, '$3x−4y=−24$', 0, '2017-10-23 08:16:06', '2017-10-23 08:16:06', 29),
(38, '112', 1, '2017-10-23 08:17:02', '2017-10-23 08:17:02', 30),
(39, '108  \r\n', 0, '2017-10-23 08:17:16', '2017-10-23 08:17:16', 30),
(40, '122\r\n', 0, '2017-10-23 08:17:27', '2017-10-23 08:17:27', 30),
(41, '  121\r\n', 0, '2017-10-23 08:17:35', '2017-10-23 08:17:35', 30),
(42, '511', 0, '2017-10-23 08:18:36', '2017-10-23 08:18:36', 31),
(43, '256  \r\n', 0, '2017-10-23 08:18:41', '2017-10-23 08:18:41', 31),
(44, '512\r\n', 1, '2017-10-23 08:18:50', '2017-10-23 08:18:50', 31),
(45, '255', 0, '2017-10-23 08:18:59', '2017-10-23 08:18:59', 31),
(46, '6', 0, '2017-10-23 08:27:51', '2017-10-23 08:27:51', 32),
(47, '  7\r\n', 0, '2017-10-23 08:28:05', '2017-10-23 08:28:05', 32),
(48, '  8\r\n', 1, '2017-10-23 08:28:12', '2017-10-23 08:28:12', 32),
(49, 'None of the above', 0, '2017-10-23 08:28:26', '2017-10-23 08:28:26', 32),
(50, '63\r\n', 0, '2017-10-23 08:29:16', '2017-10-23 08:29:16', 33),
(51, '64', 1, '2017-10-23 08:29:25', '2017-10-23 08:29:25', 33),
(52, '65\r\n', 0, '2017-10-23 08:29:36', '2017-10-23 08:29:36', 33),
(53, '66  \r\n', 0, '2017-10-23 08:29:40', '2017-10-23 08:29:40', 33),
(54, 'Equilateral Triangle', 0, '2017-10-23 08:30:39', '2017-10-23 08:30:39', 34),
(55, 'Scalene Triangle', 0, '2017-10-23 08:30:50', '2017-10-23 08:30:50', 34),
(56, 'Right Triangle \r\n', 0, '2017-10-23 08:31:03', '2017-10-23 08:31:03', 34),
(57, '  Isosceles Triangle\r\n', 1, '2017-10-23 08:31:31', '2017-10-23 08:31:31', 34),
(58, '41', 1, '2017-10-23 08:34:32', '2017-10-23 08:34:32', 35),
(59, '15\r\n', 0, '2017-10-23 08:34:47', '2017-10-23 08:34:47', 35),
(60, '24  \r\n', 0, '2017-10-23 08:34:54', '2017-10-23 08:34:54', 35),
(61, '38', 0, '2017-10-23 08:35:01', '2017-10-23 08:35:01', 35),
(62, '$6x+4 meters$\r\n', 0, '2017-10-23 08:36:11', '2017-10-23 08:36:11', 36),
(63, '  $\\frac{3}{50}x+\\frac{1}{25} meters$\r\n', 1, '2017-10-23 08:37:37', '2017-10-23 08:37:37', 36),
(64, '$3x+2 meters$', 0, '2017-10-23 08:37:59', '2017-10-23 08:37:59', 36),
(65, '$\\frac{3}{5}x+\\frac{2}{5} meters$\r\n', 0, '2017-10-23 08:41:04', '2017-10-23 08:41:04', 36),
(66, '$tan^2(x)$', 0, '2017-10-23 08:44:14', '2017-10-23 08:44:14', 37),
(67, '    $−cot^2(x)$\r\n\r\n\r\n', 1, '2017-10-23 08:44:35', '2017-10-23 08:44:58', 37),
(68, '    $−tan^2(x)$\r\n\r\n', 0, '2017-10-23 08:45:17', '2017-10-23 08:45:23', 37),
(69, '$cot^2(x)$\r\n', 0, '2017-10-23 08:45:45', '2017-10-23 08:45:45', 37),
(70, '    $2\\sqrt{110} sq. m$\r\n\r\n', 1, '2017-10-23 08:50:46', '2017-10-23 09:04:55', 38),
(71, '  $2\\sqrt{112} sq. m$  \r\n', 0, '2017-10-23 08:51:05', '2017-10-23 08:51:05', 38),
(72, ' $3\\sqrt{109} sq. m$\r\n', 0, '2017-10-23 08:51:27', '2017-10-23 08:51:27', 38),
(73, '   $3\\sqrt{111} sq. m$', 0, '2017-10-23 08:52:09', '2017-10-23 08:52:09', 38),
(74, '66', 0, '2017-10-23 09:59:15', '2017-10-23 09:59:15', 40),
(75, '78', 1, '2017-10-23 10:01:31', '2017-10-23 10:01:31', 40),
(76, '55  \r\n', 0, '2017-10-23 10:01:39', '2017-10-23 10:01:39', 40),
(77, '    315 meters\r\n\r\n', 1, '2017-10-23 10:02:52', '2017-10-23 10:03:43', 41),
(78, '370 meters\r\n', 0, '2017-10-23 10:03:02', '2017-10-23 10:03:02', 41),
(79, '336 meters', 0, '2017-10-23 10:03:18', '2017-10-23 10:03:18', 41),
(80, '454 meters  \r\n', 0, '2017-10-23 10:03:27', '2017-10-23 10:03:27', 41),
(81, '11.47 and 2.53\r\n', 0, '2017-10-23 10:06:18', '2017-10-23 10:06:18', 42),
(82, '11. 47', 1, '2017-10-23 10:06:33', '2017-10-23 10:06:33', 42),
(83, '2.53\r\n', 0, '2017-10-23 10:06:45', '2017-10-23 10:06:45', 42),
(84, 'None of the above\r\n', 0, '2017-10-23 10:06:57', '2017-10-23 10:06:57', 42),
(85, '15 seats\r\n', 0, '2017-10-23 10:07:36', '2017-10-23 10:07:36', 43),
(86, '16 seats\r\n', 0, '2017-10-23 10:08:01', '2017-10-23 10:08:01', 43),
(87, '  17 seats  \r\n', 0, '2017-10-23 10:15:34', '2017-10-23 10:15:34', 43),
(88, '  18 seats\r\n', 1, '2017-10-23 10:15:44', '2017-10-23 10:15:44', 43),
(89, '$2x^4+3x^2+8$', 0, '2017-10-23 10:22:53', '2017-10-23 10:22:53', 44),
(90, '$−2x^4+3x^2−8$', 0, '2017-10-23 10:23:23', '2017-10-23 10:23:23', 44),
(91, '$2x^4−3x^4−8$', 0, '2017-10-23 10:23:44', '2017-10-23 10:23:44', 44),
(92, '$−2x^4+3x^2+8$  \r\n', 1, '2017-10-23 10:24:05', '2017-10-23 10:24:05', 44),
(93, '  3 cans\r\n', 0, '2017-10-23 10:24:39', '2017-10-23 10:24:39', 45),
(94, '  4 cans\r\n', 0, '2017-10-23 10:24:46', '2017-10-23 10:24:46', 45),
(95, '5 cans  \r\n', 1, '2017-10-23 10:24:53', '2017-10-23 10:24:53', 45),
(96, '6 cans', 0, '2017-10-23 10:25:06', '2017-10-23 10:25:06', 45),
(97, '  30\r\n\r\n', 1, '2017-10-23 10:25:46', '2017-10-23 10:25:52', 46),
(98, '  40\r\n', 0, '2017-10-23 10:26:02', '2017-10-23 10:26:02', 46),
(99, '45\r\n', 0, '2017-10-23 10:26:08', '2017-10-23 10:26:08', 46),
(100, '50  \r\n', 0, '2017-10-23 10:26:19', '2017-10-23 10:26:19', 46),
(101, '2 meters\r\n', 0, '2017-10-23 10:27:23', '2017-10-23 10:27:23', 47),
(102, '3 meters', 1, '2017-10-23 10:27:33', '2017-10-23 10:27:33', 47),
(103, '4 meters  \r\n', 0, '2017-10-23 10:27:41', '2017-10-23 10:27:41', 47),
(104, '5 meters\r\n', 0, '2017-10-23 10:27:47', '2017-10-23 10:27:47', 47),
(105, '2.5 meters\r\n', 0, '2017-10-23 10:28:29', '2017-10-23 10:28:29', 48),
(106, '2.3 meters  \r\n', 0, '2017-10-23 10:28:41', '2017-10-23 10:28:41', 48),
(107, '2.6 meters', 0, '2017-10-23 10:28:54', '2017-10-23 10:28:54', 48),
(108, '2.4 meters\r\n', 1, '2017-10-23 10:29:16', '2017-10-23 10:29:16', 48);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `round` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `answer` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `round`, `created_at`, `updated_at`, `user_id`, `answer`) VALUES
(18, '        1 + 1        \r\n        ', 'Easy', '2017-10-22 03:26:45', '2017-10-22 03:27:52', 1, '                          \r\n          \r\n          '),
(19, '        2 * 2\r\n        ', 'Average', '2017-10-22 03:28:07', '2017-10-22 03:28:42', 1, '                          \r\n          \r\n          '),
(20, '        2 + 2\r\n        ', 'Difficult', '2017-10-22 03:29:18', '2017-10-22 03:29:18', 1, '4          '),
(21, '<p>2 - 3</p>\r\n', 'Easy', '2017-10-22 07:20:30', '2017-10-22 07:21:12', 1, NULL),
(22, '<p>6 + 6</p>\r\n', 'Easy', '2017-10-22 07:21:28', '2017-10-22 07:23:23', 1, NULL),
(23, '        7^2\r\n        ', 'Average', '2017-10-22 07:25:37', '2017-10-22 07:26:11', 1, NULL),
(24, '<p>2 + 5</p>\r\n', 'Average', '2017-10-22 07:26:58', '2017-10-22 07:27:24', 1, NULL),
(25, '        \r\n        sINO ANG AK AN', 'Easy', '2017-10-22 21:43:52', '2017-10-22 21:43:52', 10, NULL),
(26, '        \r\n        sdfsdafsdfsd', 'Easy', '2017-10-23 07:21:58', '2017-10-23 07:21:58', 1, '             \r\n          '),
(27, '        $\\sum_{i=0}^n i^2 = \\frac{(n^2+n)(2n+1)}{6}$\r\n        \r\n        ', 'Easy', '2017-10-23 07:55:22', '2017-10-23 07:55:32', 1, '                          \r\n          \r\n          '),
(28, '                                What is the simplified form of the expression  $(6x^4+4x^3−2x^2+5)−(3x^4−2x^3+x+4)$?\r\n        \r\n        \r\n        \r\n        \r\n        ', 'Easy', '2017-10-23 08:06:25', '2017-10-23 08:10:56', 1, '                                                                 \r\n          \r\n          \r\n          \r\n          \r\n          '),
(29, '                        A line has a slope of $\\frac{3}{4}$ and passes through the point $(−4,2)$. Find the equation of the line in standard form?\r\n        \r\n        \r\n        ', 'Easy', '2017-10-23 08:13:38', '2017-10-23 08:16:11', 1, '                                                    \r\n          \r\n          \r\n          \r\n          '),
(30, '        An examination consists of three parts. In part 1, a student is required to answer 3 out of 4 questions. In part 2, a student must answer 6 out of 8 questions and all questions must be answered in part 3. How many possible choices of questions does the student have?       \r\n        \r\n        ', 'Easy', '2017-10-23 08:16:44', '2017-10-23 08:17:39', 1, '                          \r\n          \r\n          '),
(31, '        $S$ is a set containing exactly 9 elements. How many subsets does $S$ have?\r\n\r\n        \r\n        ', 'Easy', '2017-10-23 08:18:15', '2017-10-23 08:19:02', 1, '                          \r\n          \r\n          '),
(32, '        If the five-digit number $54N52$ is divisible by $28$, find $N$.\r\n        ', 'Easy', '2017-10-23 08:27:37', '2017-10-23 08:28:30', 1, '                          \r\n          \r\n          '),
(33, '        How many multiples of 3 are there between 6 and 200?\r\n\r\n        ', 'Easy', '2017-10-23 08:29:04', '2017-10-23 08:29:43', 1, '                          \r\n          \r\n          '),
(34, '        What kind of triangle is formed by joining the points $(-3, 5)$, $(6, 3)$ and $(-1, -3)$?\r\n        \r\n        ', 'Easy', '2017-10-23 08:30:23', '2017-10-23 08:31:47', 1, '                          \r\n          \r\n          '),
(35, '        A pentagon has interior angles of $(3x+15)°$, $(3x)°$, $128°$, $(x)°$ and $110°$. What is the value of $x$?\r\n        \r\n        \r\n        \r\n        ', 'Easy', '2017-10-23 08:32:46', '2017-10-23 08:35:05', 1, '                                                    \r\n          \r\n          \r\n          \r\n          '),
(36, '                If the perimeter of a rectangle is $18x+24 cm$ and its width is $3x+8 cm$, what is its length in meters?\r\n        \r\n        ', 'Easy', '2017-10-23 08:35:36', '2017-10-23 08:41:26', 1, '                                       \r\n          \r\n          \r\n          '),
(37, '                        Simplify:     $\\frac{cot(x)cos(x)}{tan(−x)sin⁡(π2−x)}$.\r\n        \r\n        \r\n        ', 'Easy', '2017-10-23 08:42:16', '2017-10-23 08:45:48', 1, '                                       \r\n          \r\n          \r\n          '),
(38, '                                Find the area of a triangle with sides $6 m$, $7 m$ and $9 m$.\r\n        \r\n        \r\n        \r\n        \r\n        ', 'Average', '2017-10-23 08:49:23', '2017-10-23 09:05:16', 1, '                                                                 \r\n          \r\n          \r\n          \r\n          \r\n          '),
(39, 'Two balls are drawn from a jar containing $5$ red, $4$ blue and $7$ yellow balls. What is the probability that both balls are the same color if the first ball is not replaced?', 'Difficult', '2017-10-23 08:55:09', '2017-10-23 08:55:09', 1, '$\\frac{37}{120}$          \r\n          '),
(40, '        If there are 13 people in a party, and everyone shakes hand with everybody, how many handshakes were made?        \r\n        \r\n        ', 'Average', '2017-10-23 09:59:02', '2017-10-23 10:01:46', 1, '                          \r\n          \r\n          '),
(41, '                        A boat is $500$ meters from the base of cliff. Keanu, who is sitting in the boat, notices that angle of elevation to the top of the cliff is $32°15$. How high is the cliff? (Give your answer to the nearest whole number.)\r\n        \r\n        \r\n        \r\n        \r\n        ', 'Average', '2017-10-23 10:02:40', '2017-10-23 10:03:52', 1, '                                                    \r\n          \r\n          \r\n          \r\n          '),
(42, '                                                Solve $\\sqrt{2x−5}−\\sqrt{x−1}=1$.        \r\n        \r\n        \r\n        \r\n        \r\n        \r\n        \r\n        ', 'Average', '2017-10-23 10:04:26', '2017-10-23 10:07:05', 1, '                                                                                           \r\n          \r\n          \r\n          \r\n          \r\n          \r\n          \r\n          '),
(43, '                A conference hall has 35 rows of seats. The last row contains 120 seats. If each row has three fewer seats than the row behind it, how many seats are there in the first row?       \r\n        \r\n        \r\n        ', 'Average', '2017-10-23 10:07:23', '2017-10-23 10:15:53', 1, '                                       \r\n          \r\n          \r\n          '),
(44, '        $P$ is a polynomial such that $P(x^2+1)=−2x^4+5x^2+6$. Find $P(−x^2+3)$.    \r\n        \r\n        ', 'Average', '2017-10-23 10:21:10', '2017-10-23 10:24:08', 1, '                          \r\n          \r\n          '),
(45, '        A cylindrical water tank has a diameter of 7 meters and height of 5 meters. The curved surface and the top of the tank need to be painted. If one liter of paint covers 8 sq. cm and the paint is sold in 2-liter cans, how many cans of paint are needed to paint the tank? \r\n        \r\n        ', 'Average', '2017-10-23 10:24:28', '2017-10-23 10:25:11', 1, '                          \r\n          \r\n          '),
(46, '        Twenty-four of the students in Mary’s class are good at programming. Sixteen students are good in Math and ten are both good at programming and English. How many students are good in programming or Math or both?\r\n        \r\n        ', 'Average', '2017-10-23 10:25:36', '2017-10-23 10:26:23', 1, '                          \r\n          \r\n          '),
(47, '        Ben throws a ball into the air. The path of the ball can be modelled by the equation $h=−3t^2+6t+2$, where $t$, in seconds, is the time from the moment the ball is thrown, and $h$, in meters, is the height of the ball above the ground. Find the difference in height between the ball at its highest point and at the point from which it is thrown.\r\n        ', 'Average', '2017-10-23 10:27:12', '2017-10-23 10:27:50', 1, '                          \r\n          \r\n          '),
(48, '        Two vertical poles of height 3 meters and 12 meters are 16 meters apart. Find the height of the intersection of the lines joining the top of each pole to the foot of the other.\r\n        \r\n        \r\n        ', 'Average', '2017-10-23 10:28:15', '2017-10-23 10:29:19', 1, '                          \r\n          \r\n          '),
(49, 'The current of a river is flowing at a steady rate of 2 kilometers per hour. A speed boat went downstream 6 km and then returned to its starting point. The entire trip took 4 hours. Find the speed of the boat in still water.\r\n        ', 'Difficult', '2017-10-23 10:29:58', '2017-10-23 10:29:58', 1, '4 kilometers per hour'),
(50, 'If $(a^2−b^2)\\sin(\\theta)+2ab\\cos(\\theta)=a^2+b^2$, find the value of $\\tan⁡(\\theta)$.', 'Difficult', '2017-10-23 10:32:33', '2017-10-23 10:32:33', 1, '$\\frac{a^2−b^2}{2ab}$     \r\n          '),
(51, 'You normally drive on the freeway between place A and place B at an average speed of 105 km/hr or 65 mi/hr, and the trip takes 2 hrs and 20 minutes. On a Wednesday morning, however, heavy traffic slows you down and you drive the same distance at an average speed of only 70 km/hr or 43 mi/hr. How much longer does the trip take.', 'Difficult', '2017-10-23 10:33:21', '2017-10-23 10:33:21', 1, ' 1 hr and 10 minutes'),
(52, 'A sphere of radius $r$ inside a cube touches  each one of the six sides of the cube. What is the volume of the cube in terms of $r$?', 'Difficult', '2017-10-23 10:34:08', '2017-10-23 10:34:08', 1, ' $8r^3$            \r\n          '),
(53, 'If 5 silver is equivalent to 1 gold, 2 gold is equal to 1 silver-gold, 2 silver-gold is to 1 orange, 1 silver-gold and 2 orange is equivalent to 1 red, 2 red is equal to 1 violet, 2 violet is like having 1 green, and 1 violet with 2 green is equal to a yellow, what is; 5 silver, 3 gold, 3 silver-gold, 5 orange, 1 red, 1 violet, and 1 green?     \r\n        ', 'Difficult', '2017-10-23 10:34:53', '2017-10-23 10:34:53', 1, '1 yellow             \r\n          '),
(54, 'An integer from 100 through 999, inclusive, is to be selected at random. What is the probability that the number chosen will have 0 at least 1 digit?        \r\n        ', 'Difficult', '2017-10-23 10:35:37', '2017-10-23 10:35:37', 1, '$\\frac{171}{900}$             \r\n          '),
(55, ' If $75%$ of $m$ is equal to $k$ percent of $25$, where $k > 0$, what is the value of $\\frac{m}{k}$?\r\n\r\n        ', 'Difficult', '2017-10-23 10:37:07', '2017-10-23 10:37:07', 1, '$\\frac{1}{3}$           \r\n          '),
(56, 'One day, a person went to a horse racing area. Instead of counting the number of humans an horses, he counted 74 heads and 196 legs. How many humans and horses were there?\r\n        ', 'Difficult', '2017-10-23 10:37:41', '2017-10-23 10:37:41', 1, '24 horses, 50 humans     '),
(57, 'In a city, three quarters of the men are married to four-fifths of the women. What is the ratio of men to women in the city?', 'Difficult', '2017-10-23 10:38:09', '2017-10-23 10:38:09', 1, '16:15             \r\n          ');

-- --------------------------------------------------------

--
-- Table structure for table `question_quiz`
--

CREATE TABLE `question_quiz` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `quiz_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `question_quiz`
--

INSERT INTO `question_quiz` (`id`, `question_id`, `quiz_id`, `created_at`, `updated_at`) VALUES
(3, 20, 1, NULL, NULL),
(4, 19, 1, NULL, NULL),
(5, 18, 1, NULL, NULL),
(6, 24, 1, NULL, NULL),
(7, 23, 1, NULL, NULL),
(8, 22, 1, NULL, NULL),
(9, 21, 1, NULL, NULL),
(11, 28, 2, NULL, NULL),
(12, 37, 2, NULL, NULL),
(13, 36, 2, NULL, NULL),
(14, 35, 2, NULL, NULL),
(15, 34, 2, NULL, NULL),
(16, 33, 2, NULL, NULL),
(17, 32, 2, NULL, NULL),
(18, 31, 2, NULL, NULL),
(19, 30, 2, NULL, NULL),
(20, 29, 2, NULL, NULL),
(21, 52, 2, NULL, NULL),
(22, 51, 2, NULL, NULL),
(23, 50, 2, NULL, NULL),
(24, 49, 2, NULL, NULL),
(25, 48, 2, NULL, NULL),
(26, 47, 2, NULL, NULL),
(27, 46, 2, NULL, NULL),
(28, 44, 2, NULL, NULL),
(29, 45, 2, NULL, NULL),
(30, 43, 2, NULL, NULL),
(31, 42, 2, NULL, NULL),
(32, 41, 2, NULL, NULL),
(33, 40, 2, NULL, NULL),
(34, 38, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quizzes`
--

CREATE TABLE `quizzes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `schedule` date DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `quiz_type` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `time_limit` int(11) DEFAULT NULL,
  `curr_question` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `curr_round` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Easy',
  `status` int(11) NOT NULL DEFAULT '0',
  `showing` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `quizzes`
--

INSERT INTO `quizzes` (`id`, `name`, `created_at`, `updated_at`, `schedule`, `user_id`, `quiz_type`, `time_limit`, `curr_question`, `curr_round`, `status`, `showing`) VALUES
(1, 'Elementary Math', '2017-10-22 01:10:30', '2017-10-23 07:50:06', '2017-10-13', 1, 1, NULL, 2, 'Average', 1, 1),
(2, 'Math Olympiad 2017', '2017-10-23 06:20:52', '2017-10-23 10:49:35', '2017-10-24', 1, 1, NULL, 1, 'Difficult', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_privacies`
--

CREATE TABLE `quiz_privacies` (
  `id` int(10) UNSIGNED NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `user_email` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `quiz_privacies`
--

INSERT INTO `quiz_privacies` (`id`, `quiz_id`, `user_email`, `created_at`, `updated_at`) VALUES
(1, 1, 'maria@yahoo.com', '2017-10-22 03:53:02', '2017-10-22 03:53:02'),
(2, 1, 'ferlspenido@gmail.com', '2017-10-22 04:54:34', '2017-10-22 04:54:34'),
(3, 1, 'xia@yahoo.com', '2017-10-22 05:19:45', '2017-10-22 05:19:45'),
(4, 1, 'etan@gmail.com', '2017-10-22 07:27:47', '2017-10-22 07:27:47'),
(5, 1, 'kjsbar@gmail.com', '2017-10-22 07:27:47', '2017-10-22 07:27:47'),
(6, 1, 'rbalena@gmail.com', '2017-10-22 07:27:47', '2017-10-22 07:27:47'),
(7, 1, 'dhizon@gmail.com', '2017-10-22 07:27:47', '2017-10-22 07:27:47'),
(8, 2, 'maria@yahoo.com', '2017-10-23 08:56:06', '2017-10-23 08:56:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Gerardo', 'gaabantao1@up.edu.ph', '$2y$10$gKD/QbRgJ7jxkrQNyOR1Ge89v1wCDJDxhjfObQvjujfpgQXH9aJOy', 'maDoCHdW5ZS5X9LU8JinClKGiQwZH2BjllbErAMKYrs0dDnzgPtV5BT0Wn8v', '2017-10-22 01:10:21', '2017-10-22 07:17:20'),
(2, 'Maria', 'maria@yahoo.com', '$2y$10$IihDetW3soklNy585QlVJOwFqQBMFWt9qwN180FPHK5g9Q4roU0w6', 'GU7fgfnfEZoDDqpicEv6nlAsAvwbab1h3MiO3FMz7qLurkBe1nxLQEWQxMDv', '2017-10-22 03:38:30', '2017-10-22 05:18:26'),
(3, 'Ferlie Mae Penido', 'ferlspenido@gmail.com', '$2y$10$2qBKy71h10Y7mKmVqEmaGOubmCfegwP3ZM.MCJlGP7EhtSgt5FuHy', NULL, '2017-10-22 04:54:24', '2017-10-22 04:54:24'),
(4, 'Xia', 'xia@yahoo.com', '$2y$10$DnFs71auNwMvZaR2YKQGUuuPj4Z.mHW72AG1AFdtMFy9oDBLowO.u', NULL, '2017-10-22 05:18:51', '2017-10-22 05:18:51'),
(5, 'Edward Tan', 'etan@gmail.com', '$2y$10$3/yIvSWMoZRi9YsyO7/bj.HLZ06wa02g2nH8EL6GjTjSgaMH9x9Wu', NULL, '2017-10-22 07:16:24', '2017-10-22 07:16:24'),
(6, 'Kristine Joyce Sabar', 'kjsbar@gmail.com', '$2y$10$YsxQgVOWDlU042hQ21SGiesKtjsg7DbJRqMa79DkGrOwdmcRT8DPa', NULL, '2017-10-22 07:17:27', '2017-10-22 07:17:27'),
(7, 'Richard Balena', 'rbalena@gmail.com', '$2y$10$7PXyO20Jz5Xp5OaoKhDM5eQYrauVmaNoZVGRasICpOUqe5mi0uRm6', NULL, '2017-10-22 07:19:47', '2017-10-22 07:19:47'),
(8, 'Daniel Hizon', 'dhizon@gmail.com', '$2y$10$MDVhHaZn/.HAoF7nPEKxhetQ5NRFE38YYUEABoUF4MX.nEhvlv0sC', NULL, '2017-10-22 07:23:29', '2017-10-22 07:23:29'),
(9, 'Anez', 'aneztoso@gwapo.ako', '$2y$10$ZgSeuW8/wFZqotRiNmYv2.lhcvv0PhBlpm7DLwlm5NLtGPlKOPDRG', 'czJJWdfWEFo48LG6fvSkxmDKbLYMYZoGJaqendJV6WDfATVTMmZFntcvGvUW', '2017-10-22 21:31:41', '2017-10-22 21:32:28'),
(10, 'none', 'none@none.com', '$2y$10$gS3xTdVzvJk1UGT49Zu8meHLhL8raE2UJUSFmlH9xI9r8aK7AH6EW', NULL, '2017-10-22 21:35:01', '2017-10-22 21:35:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lq_answers`
--
ALTER TABLE `lq_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lq_answers_question_id_foreign` (`question_id`),
  ADD KEY `lq_answers_quiz_id_foreign` (`quiz_id`),
  ADD KEY `lq_answers_user_id_foreign` (`user_id`),
  ADD KEY `lq_answers_answer_id_foreign` (`answer_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `options_question_id_foreign` (`question_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_user_id_foreign` (`user_id`);

--
-- Indexes for table `question_quiz`
--
ALTER TABLE `question_quiz`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_quiz_question_id_foreign` (`question_id`),
  ADD KEY `question_quiz_quiz_id_foreign` (`quiz_id`);

--
-- Indexes for table `quizzes`
--
ALTER TABLE `quizzes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quizzes_user_id_foreign` (`user_id`);

--
-- Indexes for table `quiz_privacies`
--
ALTER TABLE `quiz_privacies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lq_answers`
--
ALTER TABLE `lq_answers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `question_quiz`
--
ALTER TABLE `question_quiz`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `quizzes`
--
ALTER TABLE `quizzes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `quiz_privacies`
--
ALTER TABLE `quiz_privacies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lq_answers`
--
ALTER TABLE `lq_answers`
  ADD CONSTRAINT `lq_answers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lq_answers_quiz_id_foreign` FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lq_answers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `options`
--
ALTER TABLE `options`
  ADD CONSTRAINT `options_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question_quiz`
--
ALTER TABLE `question_quiz`
  ADD CONSTRAINT `question_quiz_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`),
  ADD CONSTRAINT `question_quiz_quiz_id_foreign` FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`);

--
-- Constraints for table `quizzes`
--
ALTER TABLE `quizzes`
  ADD CONSTRAINT `quizzes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
