<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAnswerIdForeignLqanswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lq_answers', function (Blueprint $table) {
            $table->dropForeign(['answer_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lq_answers', function (Blueprint $table) {
            $table->foreign('answer_id')->references('options')->on('id');        
        });
    }
}
