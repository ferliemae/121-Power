<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\OptionFormRequest;

use App\Option;
use Illuminate\Http\Request;

class OptionController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$options = Option::orderBy('id', 'desc')->paginate(10);

		return view('options.index', compact('options'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$option = new Option();

		return view('options.create', compact('option'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(OptionFormRequest $request, $question)
	{
		$option = new Option();

		$option->value = $request->input("value");
        $option->answer = $request->input("answer");
        $option->question_id = $question;

		$option->save();

		return redirect()->route('questions.edit', $question )->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$option = Option::findOrFail($id);

		return view('options.show', compact('option'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$option = Option::findOrFail($id);

		return view('options.edit', compact('option'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(OptionFormRequest $request, $question, $option)
	{
		$option = Option::findOrFail($option);

		$option->value = $request->input("value");
        $option->answer = $request->input("answer");
        $option->question_id = $question;

		$option->save();
		return redirect()->route('questions.edit', $question )->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($question, $option)
	{
		$option = Option::findOrFail($option);
		$option->delete();
		return redirect()->route('questions.edit', $question )->with('message', 'Item deleted successfully.');
	}

}
