<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizSection extends Model
{
    protected $table = "quiz_sections";

    public function quiz()
    {
    	return $this->belongsTo('App\Quiz');
    }
    public function questions()
    {
    	return $this->hasMany('App\SectionQuestion');
    }

}
