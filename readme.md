CS Quiz App is a web application for delivering quizzes online!

To install:

- `git clone --recursive`, DON'T FORGET the recursive flag to make sure you get everything!~
- `composer install`
- edit `env.example` file to your liking and `php artisan key:generate` to generate the key of the app for cryptographic related functionalities
- `php artisan migrate` to create the database

- edit your web server configuration file if necessary. 
